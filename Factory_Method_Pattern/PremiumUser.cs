﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Factory_Method_Pattern
{
    //Clase para la instanciacion de Usuarios Premiums
    public class PremiumUser : User , IIngresarDatos
    {
        //constructor con valores predefinidos para el ejemplo
        public PremiumUser()
        {
            this.codigo = 3;
            this.nombre = IngresarDatos();            
        }
                
        //metodo polimorfico implementado de la interface IIngresarDatos
        public string IngresarDatos()
        {
            Console.WriteLine("        ");
            Console.WriteLine("Ingrese su nombre y su apellido ");
            Console.WriteLine("        ");
            string nombreUsuario = Console.ReadLine();
            return nombreUsuario;
        }

        //metodo polimorfico por clase abstracta User
        public override void MostrarDatos()
        {
            Console.WriteLine("        ");
            Console.WriteLine("!!!SE HA CREADO UN USUARIO PREMIUM!!!");
            Console.WriteLine("Codigo de usuario : " + this.codigo + "\n" + this.nombre + " Tiene Acceso Total al Contenido");
            Console.WriteLine("        ");
        }

    }
}
