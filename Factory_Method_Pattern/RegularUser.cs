﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Method_Pattern
{
    //Clase para la instanciacion de Usuarios Regulares
    public class RegularUser : User, IIngresarDatos
    {
        //constructor con valores predefinidos para el ejemplo
        public RegularUser()
        {
            this.codigo = 2;
            this.nombre = IngresarDatos();
        }

        //metodo polimorfico implementado de la interface IIngresarDatos
        public string IngresarDatos()
        {
            Console.WriteLine("        ");
            Console.WriteLine("Ingrese solo su nombre");
            Console.WriteLine("        ");
            string nombreUsuario = Console.ReadLine();
            return nombreUsuario;
        }

        //metodo polimorfico por clase abstracta User
        public override void MostrarDatos()
        {
            Console.WriteLine("        ");
            Console.WriteLine("!!!SE HA CREADO UN USUARIO REGULAR!!!");
            Console.WriteLine("Codigo de usuario : " + this.codigo + "\n" + this.nombre + " Tiene Acceso al Contenido Estandar");
            Console.WriteLine("        ");
        }

    }
}
