﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Method_Pattern
{    
    //Clase abstracta padre 
    public abstract class User
    {        
        protected int codigo { get; set; }
        protected string nombre { get; set; }
                
        //metodo polimorfico que muestra los datos del nuevo usuario
        public abstract void MostrarDatos ();
                
    }
}
