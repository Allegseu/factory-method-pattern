﻿using System;

namespace Factory_Method_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //se instancia una sola vez el objeto de tipo Factory
            //el cual se encargara de instanciar los diferentes tipos de usuarios
            //dependiendo a lo que se le solicite
            UserFactory fabrica = new UserFactory();

            UserFactoryProm fabricaProm = new UserFactoryProm();


            //menu de opciones
            int opcion = 0;
            while (opcion != 2)
            {
                Menu();
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        //se instancia un tipo de usuario por medio de la respectiva fabrica 
                        User nuevoUsuario = fabrica.CrearUsuario(IngresoValorSuscripcion());
                        //ya creado el nuevo usuario se accede a los metodos particulares
                        //del usuario
                        nuevoUsuario.MostrarDatos();
                        break;                    
                    case 2:
                        Console.WriteLine("Esta saliendo de la Aplicacion");
                        opcion = 2;
                        break;
                    default:
                        break;
                }
            }
            Console.ReadKey();
        }
        //metodo por el cual el usuario ingresara el valor deS la suscripcion
        static decimal IngresoValorSuscripcion()
        {
            Console.WriteLine("        ");
            Console.WriteLine("Ingrese el valor de su suscripcion");
            Console.WriteLine("        ");
            decimal suscripcion = Convert.ToDecimal(Console.ReadLine());
            return suscripcion;
        }        
        static void Menu()
        {
            Console.WriteLine("Menu de opciones:");
            Console.WriteLine("        ");
            Console.WriteLine("1 ===> Crear un Nuevo Usuario");
            Console.WriteLine("2 ===> SALIR");
            Console.WriteLine("Escoja el numero segun la opcion que vaya a realizar y pulse ENTER");
            Console.WriteLine("        ");
        }
    }
}
