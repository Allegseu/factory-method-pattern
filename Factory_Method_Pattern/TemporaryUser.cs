﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Method_Pattern
{
    //Clase para la instanciacion de Usuarios Temporales
    public class TemporaryUser : User
    {
        //constructor con valores predefinidos para el ejemplo
        public TemporaryUser()
        {
            this.codigo = 1;
            this.nombre = "Usuario temporal";
        }
        //metodo polimorfico por clase abstracta User
        public override void MostrarDatos()
        {
            Console.WriteLine("        ");
            Console.WriteLine("!!!SE HA CREADO UN USUARIO TEMPORAL!!!");
            Console.WriteLine("Codigo de usuario : " + this.codigo + "\nEl " + this.nombre + " Tiene Acceso al Contenido por 7 dias");
            Console.WriteLine("        ");
        }
    }
}
