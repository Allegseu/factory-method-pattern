﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Method_Pattern
{
    //clase encargada de crear los diferentes tipos de usuarios en epoca de promocion (Objetos)
    public class UserFactoryProm : IUserCreator
    {
        //metodo polimorfico implementado de IUserCreator
        //el metodo retorna un tipo de usuario dependiendo al parametro 
        public User CrearUsuario(decimal valor)
        {
            if (valor >= 5)
            {
                //para crear un usuario premium el valor de inscripcion debe ser mayor o igual que 5 $
                return new PremiumUser();
            }
            else
            {
                if ((valor >= 2) && (valor < 5))
                {
                    //para crear un usuario regular el valor de inscripcion 
                    //debe ser mayor o igual que 2 $ y menor que 5 $
                    return new RegularUser();
                }
                else
                {
                    if ((valor >= 0) && (valor < 2))
                    {
                        //para crear un usuario temporal el valor de inscripcion 
                        //debe ser mayor o igual que 0 $ y menor que 2 $
                        return new TemporaryUser();
                    }
                    return null;
                }
            }
        }
    }
}
