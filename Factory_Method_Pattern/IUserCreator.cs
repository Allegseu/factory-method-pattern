﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Method_Pattern
{
    //Interface desde donde se implementa a los diferentes tipos de Factorys
    //el metodo que crea las distintas instancias        
    public interface IUserCreator
    {
        //Metodo que devuelve una instancia de tipo User
        User CrearUsuario(decimal valor);
        
    }
}
