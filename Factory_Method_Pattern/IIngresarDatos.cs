﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Method_Pattern
{
    //Interface para ingresar datos de tipo string  ==> exclusivo para la clase PremiumUser y RegularUser
    interface IIngresarDatos
    {
        string IngresarDatos();
    }
}
