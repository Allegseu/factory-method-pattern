﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Method_Pattern
{
    //clase encargada de crear los diferentes tipos de usuarios (Objetos)
    public class UserFactory : IUserCreator
    {        
        //metodo polimorfico implementado de IUserCreator
        //el metodo retorna un tipo de usuario dependiendo al parametro 
        public User CrearUsuario(decimal valor)
        {
            if (valor >= 10)
            {
                //para crear un usuario premium el valor de inscripcion debe ser mayor o igual que 10 $
                return new PremiumUser();
            }
            else
            {
                if ((valor >= 5) && (valor <10))
                {
                    //para crear un usuario regular el valor de inscripcion 
                    //debe ser mayor o igual que 5 $ y menor que 10 $
                    return new RegularUser();
                }
                else{
                    if ((valor >= 1) && (valor < 5))
                    {
                        //para crear un usuario temporal el valor de inscripcion 
                        //debe ser mayor o igual que 1 $ y menor que 5 $
                        return new TemporaryUser();
                    }
                    return null ;
                }
            }
        }
    }
}
